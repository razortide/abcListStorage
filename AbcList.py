from datetime import date
abc_lists = {}

class AbcList:
    
    topic = ""
    creation_date = ""
    list_data = {"0-9":[], "A":[], "B":[], "C":[], "D":[], "E":[], "F":[], "G":[],
    "H":[], "I":[], "J":[], "K":[], "L":[], "M":[], "N":[], "O":[], "P":[],
    "Q":[], "R":[], "S":[], "T":[], "U":[], "V":[], "W":[], "X":[], "Y":[],
    "Z":[], "Ä":[], "Ö":[], "Ü":[]}
    time = 120

    def __init__(self, topic: str, date:str = date.today(), time: int = 120):
        self.topic = topic
        self.creation_date = date
        self.time = time
        if topic not in abc_lists:
            abc_lists[topic] = []
        abc_lists[topic].append(self)

    def add_term(self, term: str):
        term = term.capitalize()
        key = term[0]
        if key.isnumeric():
            key = "0-9"
        if key not in self.list_data:
            self.list_data[key] = []
        if term not in self.list_data[key]:
            self.list_data[key].append(term)
            self.list_data[key].sort()
    
    def change_term(self,  old_term: str, new_term: str):
        key = ""
        if old_term[0].isnumeric:
            key = "0-9"
        else:
            key = old_term[0]

        if old_term in self.list_data[key]:
            self.list_data[key].remove(old_term)
            self.add_term(new_term)
        else:
            print ("Term not found")


if __name__ == "__main__":
    ## Testing
    test_lists= []
    test_lists.append(AbcList("Mine"))
    test_lists.append(AbcList("Mine"))
    test_lists.append(AbcList("Other"))

    test_lists[1].date = "2022-09-13"

    test_terms = ["12Quest","Presto","FrEsKo","Üsko","ülmö","älmä"]
    for list in test_lists:
        for each in test_terms:
            list.add_term(each)

        print(list.topic)
        print(list.creation_date)
        print(list.list_data)


        
